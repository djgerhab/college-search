class CollegesService
  def initialize
    api_client = ApiClient.new("https://api.data.gov")
    @connection = api_client.connection
  end

  def search_colleges(name)
    raise ArgumentError if name.nil?
    response = @connection.get("/ed/collegescorecard/v1/schools.json") do |req|
      req.params['api_key'] = ENV['COLLEGE_API_KEY']
      req.params['school.name'] = name
      req.params['_fields'] = 'id,school.name,location'
    end

    # Typically would place this kind of logging in server logs but for simplicity of this interview app, leaving as a puts
    puts response.inspect
    to_json(response.body) if response.body
  end

  private

  def to_json(body)
    ParseJson.call(body)
  end
end