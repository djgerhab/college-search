class ParseJson
  def self.call(string)
    JSON.parse(string)
  rescue JSON::ParserError => error
    raise Errors::ParsingError, error.message
  end
end
