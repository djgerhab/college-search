let map;
let markers = [];
let infoWindow;
let bounds;

// Google Maps init function
// Note that I assigned this to the window object to get around webpacker's encapsulation
window.initMap = function() {
    const location = new google.maps.LatLng(41.850033, -87.6500523);
    const mapOptions = {
        center: location,
        mapTypeId: "terrain",
        zoom: 3
    };
    map = new google.maps.Map(document.getElementById('map'), mapOptions);
    infoWindow = new google.maps.InfoWindow();
}

// Called by AJAX callback in controller to update map with response
window.updateMap = function(response) {
    const results = response.results;
    const noResultsElClasses = document.getElementById("no-results").classList;

    clearMarkers();
    bounds = new google.maps.LatLngBounds();

    if (results && results.length) {
        // Remove no-results text is exists
        if (!noResultsElClasses.contains("d-none")) {
            noResultsElClasses.add("d-none");
        }

        // Iterate through each result and add markers
        results.forEach((result, index) => {
            const position = new google.maps.LatLng(result["location.lat"], result["location.lon"]);
            addMarker(position, result["school.name"], index + 1)
        })

        // Move the map to fit the new boundary's
        map.fitBounds(bounds);

        // Set a minimum zoom level for when there is only 1 result
        if (map.getZoom() > 14) {
            map.setZoom(14);
        }
    } else {
        // Display no-results text by removing bootstrap d-none class
        noResultsElClasses.remove("d-none");
        map.setZoom(3);
    }
}

// Adds a marker to the map and push to the array.
const addMarker = (position, name, index) => {
    const marker = new google.maps.Marker({
        label: `${index}`,
        map,
        optimized: false,
        position,
        title: `${index}. ${name}`,
    });

    // Extend the map boundary to include each marker's position
    bounds.extend(marker.position);

    // Add a click listener for each marker to set up the info window.
    marker.addListener("click", () => {
        infoWindow.close();
        infoWindow.setContent(marker.getTitle());
        infoWindow.open(marker.getMap(), marker);
    });
    markers.push(marker);
}

// Deletes all markers in the array by removing references to them.
const clearMarkers = () => {
    markers.forEach(marker => {
        marker.setMap(null);
    })
    markers = [];
}