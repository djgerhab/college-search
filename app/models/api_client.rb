require 'faraday'

class ApiClient

  def initialize(url)
    raise ArgumentError, 'url must not be blank' if url.blank?

    @connection = Faraday.new(url, options) do |connection|
      connection.adapter(:net_http)
      yield(connection) if block_given?
    end
  end

  # accessor getter for connection
  attr_reader :connection

  private

  def options
    @options ||= {
      request: {
        timeout: Rails.application.config.api_timeout,
        open_timeout: Rails.application.config.api_open_timeout
      }
    }
  end
end
