class CollegesController < ApplicationController
  def index
    unless params['name'].blank?
      response = colleges_service.search_colleges(params['name'])

      render :json => response, :callback => 'updateMap'
    end
  end

  private

  def colleges_service
    @colleges_service ||= CollegesService.new
  end
end
