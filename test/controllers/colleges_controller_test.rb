require "test_helper"

class CollegesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get colleges_url
    assert_response :success
    assert_equal "index", @controller.action_name
  end

  test "should call service and render with json when param name is defined" do
    get colleges_url, params: {name: "Harvard"}, xhr: true
    assert_response :success
    assert_equal "text/javascript", @response.media_type
  end
end
