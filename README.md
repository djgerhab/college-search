# College Search

Exercise for Entera in which a person can search for colleges and find them on a map.

## Setup
_To keep secrets out of a public repo, I utilize an environment file_
1. Obtain an API token from this [link](https://collegescorecard.ed.gov/data/documentation/)
2. Create a `.env` file with `COLLEGE_API_KEY` as the key and the obtained token from above as the value
3. Obtain an API token for Google maps and add it to the `.env` with `GOOGLE_API_KEY` as the key and the obtained token as the value
4. Run `bundle install` on this repo locally
5. Run `yarn install` on this repo locally
6. Run `rails s` to start the server

## Tests
Run tests with the following command: `bin/rails test`